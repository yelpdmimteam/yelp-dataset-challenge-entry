import json
from pprint import pprint
# [
#  {
#    "Business_Name":"name"
#    "Business_id" :"ID"
#    "Business_latitude":"Lat"
#    "Business_longitude:"Lon"
#    "Business_Stars:"Stars"
#    "Business_Categories::["cat1",....","catn"]
#    "Business_topic_words":["tw1",...,"twn"]
#  }
# ]

pprint(json.dumps({[
{'business_name':'StarBucks'
,'business_id':0,
'business_address':{'latitude':553.754,'longitude':-875.75245},
"business_stars":4,
"business_categories":["coffee-bar","restaurant"],
"business_topic_words":["food","music","family"]
}
]
}
)
)

