from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from restAPI import views

urlpatterns = [
    url(r'^businesses/$', views.business_list),
    url(r'^users/userinfo/$', views.user_info),
    url(r'^users/userinfo/similarusers/$', views.similar_users)
 ]

urlpatterns = format_suffix_patterns(urlpatterns)
