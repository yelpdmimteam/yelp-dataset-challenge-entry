import pymongo
from pymongo import MongoClient
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

import sys
sys.path.append("/home/xenia-zlati/yelp-dataset-challenge-entry/src/core")

from Entities.Business import Business
from Entities.User import User
from Classification.Classifier import Classifier
from Entities.UserGroup import UserGroup
from Entities.BusinessGroup import BusinessGroup
from Entities.Topic import Topic

@api_view(['GET'])
def business_list(request, format=None):
        user_id = request.query_params.get('user_id', None)
	client = MongoClient('localhost', 27017)
	db = client.yelp

	contents=[]
	cl=Classifier()
	for b in cl.Classify(db, user_id): 
		businessGroup=BusinessGroup.Cursor_Constructor(db.businessGroup.find_one({'businesses':{'$eq':b.Get_Business_Id()}}))	
		content={'business_name':b.Get_Name(),
	'business_id':b.Get_Business_Id(),
	'business_address':{'latitude':b.Get_Latitude(),'longitude':b.Get_Longitude()},
	'business_full_address':b.Get_Full_address(),
	'business_stars':b.Get_Stars(),
	'business_categories':b.Get_Categories(),
	#We import attributes instead of topic words	
	'business_topic_words':businessGroup.Fetch_Set_Topic_Words(db)}
	
		contents.append(content)

	response = Response(contents)
	#Cross origin resource sharing problem solving		
	response["Access-Control-Allow-Origin"] = "http://localhost:63342"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept"
    	response["Access-Control-Allow-Credentials"] = "true";
	return response

@api_view(['GET'])
def user_info(request, format=None):	
	
	user_id = request.query_params.get('user_id', None)

	client = MongoClient('localhost', 27017)
	db = client.yelp

	content={}	
	u=User.Collection_Constructor(db,user_id)
	userFriends={}
	userGroup=UserGroup.Cursor_Constructor(db.userGroup.find_one({'users':{'$eq':user_id}}))
	
	for i in u.Get_Friends():  	
		uf=User.Collection_Constructor(db,i)
		userFriends[uf.Get_Name()]=i 
	if u is not None:		
		content={'user_name':u.Get_Name(),
	'user_id':user_id,
	'user_review_count':u.Get_Review_Count(),
	'user_average_stars':u.Get_Average_Stars(),
	'user_compliments':u.Get_Compliments(),
	#need to be edited later
	'user_topic_words':userGroup.Fetch_Set_Topic_Words(db) ,
	'user_fans' : u.Get_Fans(),
	'user_friends' : userFriends,
	}

	response = Response(content)

	#Cross origin resource sharing problem solving	
	response["Access-Control-Allow-Origin"] = "http://localhost:63342"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept"
    	response["Access-Control-Allow-Credentials"] = "true";
	return response

@api_view(['GET'])
def similar_users(request, format=None):	
	
	user_id = request.query_params.get('user_id', None)

	client = MongoClient('localhost', 27017)
	db = client.yelp
	content={}
	
	userGroup=UserGroup.Cursor_Constructor(db.userGroup.find_one({'users':{'$eq':user_id}}))	

	similar_users=userGroup.Get_Firstk_Users_In_Group(db, user_id,30)
	sim={}	
	for i in similar_users:  	
		sim[i.Get_Name()]=i.Get_User_id()
		
	
	content={'similar_users' : sim
	}

	response = Response(content)
	

	#Cross origin resource sharing problem solving	
	response["Access-Control-Allow-Origin"] = "http://localhost:63342"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept"
    	response["Access-Control-Allow-Credentials"] = "true";
	return response





