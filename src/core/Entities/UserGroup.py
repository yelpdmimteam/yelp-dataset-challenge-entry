from User import User
import pymongo
from pymongo import MongoClient
from Topic import Topic
import random
class UserGroup:
    users=[]
    topics=[]

    @classmethod
    def Default_Constructor(cls,user,topics):
	obj=cls()
        obj.users=[]
        obj.users.append(user)
        obj.topics=topics
        return obj    
    @classmethod
    def Topic_Constructor(cls,topics):
	obj=cls()        
	obj.topics=topics
	return obj

    @classmethod
    def Cursor_Constructor (cls,u):
	obj=cls()
	obj.topics=u['topics']
	obj.users=u['users']
	return obj
    
    @classmethod
    def Collection_Constructor(cls,db,usergroupId):
	obj=cls()
	u=db.userGroup.find_one({"_id":usergroupId})	
	obj.topics=u['topics']
	obj.users=u['users']
	return obj
   

    
    def Get_Topics(self):
	return self.topics

    def Topics_Match(self,new_topics):
	if len(new_topics)!=len(self.topics):
		return False
	for t in new_topics:
		if t not in self.topics:
			return False
	return True	
    
    def Get_User_Ids(self):
	return self.users
    
    def Get_User(self,user_id):
        for u in users:
            if u==user_id:
                return u 

    def Get_User(self,db,user_id):
        print(self.users)
        for u in self.users:
            if u==user_id:
                u=User.Collection_Constructor(db,user_id)
                return u
            
    def Add_User(self,user_id):       
	self.users.append(user_id)
	

    def Print_Group(self):
        for u in self.users:
            u=User.Collection_Constructor(db,u)
            print(u.Get_Name())

    def Fetch_User_Objects(self,db):
        userobjects=[]
        for u in self.users:
            userobjects.append(User.Collection_Constructor(db,u))
        return userobjects   

    def Fetch_Set_Topic_Words(self,db):
	topicwords=[]
	for t in self.topics:
		topicx=Topic.Collection_Constructor(db,t).Get_Topic_Words()
		for tw in topicx:		
            		topicwords.append(tw)
	return set(topicwords)

    def Get_Rest_Users_In_Group(self, db, user_id):        
	userobjects=[]
        for u in self.users:
	    if u!=user_id:
    	        userobjects.append(User.Collection_Constructor(db,u))
        return userobjects 

    """
    def Get_Firstk_Users_In_Group(self, db, user_id,k):        
	userobjects=[]
	i=0
        for u in self.users:
	    if u!=user_id:
    	        userobjects.append(User.Collection_Constructor(db,u))
	    	
	    if i==k:
		break
	    i+=1	
        return userobjects 

    """
    def Get_Firstk_Users_In_Group(self, db, user_id,k):        
	userobjects=[]
	userobjectsId=[]
	r=len(self.users)
	if k>r:
		for u in self.users:
	    		if u!=user_id:
    	       			userobjects.append(User.Collection_Constructor(db,u))
		return userobjects
	    	
	while len(userobjectsId)<k :
		rand = random.randint(0, r-1)
		if self.users[rand]!=user_id:
			if self.users[rand] not in userobjectsId:
				userobjectsId.append(self.users[rand])
				
        for i in range(0,k):
 		userobjects.append(User.Collection_Constructor(db,userobjectsId[i]))

	    	
	return userobjects 	
    
    def toDB(self,db):
	usergroup={"topics": [topic for topic in self.topics],
		    "users": [user for user in self.users]}
	db.userGroup.insert_one(usergroup)



