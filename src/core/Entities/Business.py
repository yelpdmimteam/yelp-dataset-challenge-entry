import pymongo
from pymongo import MongoClient

class Business:

	business_id='none'
	name='none'
	neighborhoods=[]
	full_address='none'
	city='none'
	state='none'
	latitude=0
	longitude=0
	stars=0
	review_count=0
	categories=[]
	attributes={}

	@classmethod
	def Default_Constructor(cls,business_id,name,neighborhoods,full_address,city,state,latitude,longitude,stars,review_count,categories,attributes):
		obj=cls()
                obj.business_id=business_id
                obj.name=name
                obj.neighborhoods=neighborhoods
                obj.full_address=full_address
                obj.city=city
                obj.state=state
                obj.latitude=latitude
                obj.longitude=longitude
                obj.stars=stars
                obj.review_count=review_count
                obj.categories=categories
                obj.attributes=attributes
		return obj

	@classmethod
	def Cursor_Constructor (cls,b):
		obj=cls()
		obj.business_id=b['business_id']
		obj.name=b['name']
		obj.neighborhoods=b['neighborhoods']
		obj.full_address=b['full_address']
		obj.city=b['city']
		obj.state=b['state']
		obj.latitude=b['latitude']
		obj.longitude=b['longitude']
		obj.stars=b['stars']
		obj.review_count=b['review_count']
		obj.categories=b['categories']
		obj.attributes=b['attributes']
		return obj
	
	@classmethod
	def Collection_Constructor (cls,db,business_id):
		obj=cls()               
		businesses=db.businesses
                b=businesses.find_one({"business_id":business_id})
                obj.business_id=business_id
                obj.name=b['name']
                obj.neighborhoods=b['neighborhoods']
                obj.full_address=b['full_address']
                obj.city=b['city']
                obj.state=b['state']
                obj.latitude=b['latitude']
                obj.longitude=b['longitude']
                obj.stars=b['stars']
                obj.review_count=b['review_count']
                obj.categories=b['categories']
                obj.attributes=b['attributes']
		return obj
         
	def Set_Business_Id(self,business_id):
		self.business_id=business_id
	def Get_Business_Id(self):
		return self.business_id


	def Set_Name(self,name):
                self.name=name
	def Get_Name(self):
                return self.name


	def Set_Neighborhoods(self,neighborhoods):
                self.neighborhoods=neighborhoods
	def Get_Neighborhoods(self):
                return self.neighborhoods


	def Set_Full_Address(self,full_address):
                self.full_address=full_address
	def Get_Full_address(self):
                return self.full_address


	def Set_City(self,city):
                self.city=city
	def Get_City(self):
                return self.city


	def Set_State(self,state):
                self.state=state
	def Get_State(self):
                return self.state


	def Set_Latitude(self,latitude):
                self.latitude=latitude
	def Get_Latitude(self):
                return self.latitude


	def Set_Longitude(self,longitude):
                self.longitude=longitude
	def Get_Longitude(self):
                return self.longitude


	def Set_Stars(self,stars):
                self.stars=stars
	def Get_Stars(self):
                return self.stars


	def Set_Review_Count(self,review_count):
                self.review_count=review_count
	def Get_Review_Count(self):
                return self.review_count


	def Set_Categories(self,categories):
                self.categories=categories
	def Get_Categories(self):
                return self.categories
                

	def Set_Attributes(self,attributes):
                self.attributes=attributes
	def Get_Attributes(self):
                return self.attributes


