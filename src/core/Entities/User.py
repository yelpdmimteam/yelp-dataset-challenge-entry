import pymongo
from pymongo import MongoClient

class User:
	
	user_id=-1
	name="none"
	review_count=0
	average_stars=0
	votes={}
	friends= []
	compliments={}
	fans=[]

	@classmethod
	def Default_Constructor(cls,user_id,name,review_count,average_stars,friends,compliments,fans):
		obj=cls()		
		obj.name = name
		obj.user_id=user_id
		obj.review_count=review_count
		obj.average_stars=average_stars
		obj.friends=friends
		obj.compliments=compliments
		obj.fans=fans
		return obj

	@classmethod
	def Collection_Constructor (cls,db,user_id):
		obj=cls()		
		user=db.user
		u=user.find_one({"user_id":user_id})	
		obj.name =u['name']
		obj.user_id=user_id
		obj.review_count=u['review_count']
		obj.average_stars=u['average_stars']
		obj.friends=u['friends']
		obj.compliments=u['compliments']
		obj.fans=u['fans']
		return obj

	@classmethod
	def Cursor_Constructor (cls,b):
		obj=cls()		
		obj.name =u['name']
		obj.user_id=u['user_id']
		obj.review_count=u['review_count']
		obj.average_stars=u['average_stars']
		obj.friends=u['friends']
		obj.compliments=u['compliments']
		obj.fans=u['fans']
		return obj
		
	
	def Set_User_id(self,user_id):
		self.user_id=user_id
	def Get_User_id(self):
		return self.user_id


	def Set_Name(self,name):
		self.name=name
	def Get_Name(self):
		return self.name


	def Set_Review_Count(self,review_count):
		self.review_count=review_count
	def Get_Review_Count(self):
		return self.review_count

	def Set_Average_Stars(self,average_stars):
		self.average_stars=average_stars
	def Get_Average_Stars(self):
		return self.average_stars

	def Set_Friends(self,friends):
		self.friends=friends
	def Get_Friends(self):
		return self.friends[0:20]

	def Set_Compliments(self,compliments):
		self.compliments=compliments
	def Get_Compliments(self):
		return self.compliments
	
	def Set_Fans(self,fans):
		self.fans=fans
	def Get_Fans(self):
		return self.fans

	

