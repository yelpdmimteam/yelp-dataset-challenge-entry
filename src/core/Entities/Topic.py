import pymongo
from pymongo import MongoClient

class Topic:
	
	@classmethod
	def Default_Constructor(cls,topic_id,topic_words):
		obj=cls()
		obj.topic_id=topic_id
		obj.topic_words=topic_words
		return obj

	@classmethod
	def Collection_Constructor (cls,db,topic_id):
		obj=cls()               
		topic=db.topic
                t=topic.find_one({"topic_id":topic_id})
                obj.topic_words=t['topic_words']
		return obj
         
	def Get_Topic_Words(self):
		
		return self.topic_words
	
	def toDB(self,db):
		t={'topic_id': self.topic_id,
			'topic_words': [topic for topic in self.topic_words]}
		db.topic.insert_one(t)
