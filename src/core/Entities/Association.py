from UserGroup import UserGroup
from BusinessGroup import BusinessGroup


class Association:

	@classmethod
	def Default_Constructor(cls,userGroupId,businessGroupId):
		obj=cls()
        	obj.userGroupId=userGroupId
        	obj.businessGroupId=businessGroupId
        	return obj    
	

	@classmethod
        def Cursor_Constructor (cls,a):
		obj=cls()
		obj.userGroupId=a['userGroupId']
		obj.businessGroupId=a['businessGroupId']
		return obj


	def toDB(self,db):
		a={'userGroupId':self.userGroupId,
				'businessGroupId':self.businessGroupId}

		db.association.insert_one(a)

	def Get_UserGroupId(self):
		return self.userGroupId

	def Get_BusinessGroupId(self):
		return self.businessGroupId
	
	def Fetch_BusinessGroup(self,db):
        	businessgroup=BusinessGroup.Collection_Constructor(db,self.businessGroupId)
		return businessgroup
		
	def Fetch_UserGroup(self,db):
        	usergroup=UserGroup.Collection_Constructor(db,self.userGroupId)
		return usergroup
