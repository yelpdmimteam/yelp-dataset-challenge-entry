from Business import Business
from Topic import Topic
import pymongo
from pymongo import MongoClient

class BusinessGroup:
    businesses=[]
    topics=[]

    
    @classmethod
    def Default_Constructor(cls,business,topics):
	obj=cls()
        obj.businesses=[]
        obj.businesses.append(business)
        obj.topics=topics
        return obj    

    @classmethod
    def Collection_Constructor(cls,db,businessgroupId):
	obj=cls()
	b=db.businessGroup.find_one({"_id":businessgroupId})	
	obj.topics=b['topics']
	obj.businesses=b['businesses']
	return obj
   


    @classmethod
    def Cursor_Constructor (cls,b):
	obj=cls()
	obj.topics=b['topics']
	obj.businesses=b['businesses']
	return obj

    def Get_Topics(self):
	return self.topics

    def Topics_Match(self,new_topics):
	if len(new_topics)!=len(self.topics):
		return False
	for t in new_topics:
		if t not in self.topics:
			return False
	return True	

    def Fetch_Set_Topic_Words(self,db):
	topicwords=[]
	for t in self.topics:
		topicx=Topic.Collection_Constructor(db,t).Get_Topic_Words()
		for tw in topicx:		
            		topicwords.append(tw)
	return set(topicwords)

    def Get_Business_Id(self,business_id):
        for b in businesses:
            if b==business_id:
                return b 

    def Get_Business(self,db,business_id):
        for b in self.businesses:
            if b==business_id:
                b=Business.Collection_Constructor(db,business_id)
                return b

    def Add_Business(self,business_id):
        self.businesses.append(business_id)

    def Print_Group(self):
        for b in self.businesses:
            b=Business.Collection_Constructor(db,b)
            

    def Fetch_Business_Objects(self,db):
        businessobjects=[]
        for b in self.businesses:
            businessobjects.append(Business.Collection_Constructor(db,b))
        return businessobjects
                         
    def toDB(self,db):
	businessgroup={'topics': [topic for topic in self.topics],
			'businesses': [business for business in self.businesses]}
	db.businessGroup.insert_one(businessgroup)

