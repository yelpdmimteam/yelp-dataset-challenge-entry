import pymongo
from pymongo import MongoClient

class Reviews:
	
	review_id='none'
	business_id='none'
	user_id='none'
	stars=0
	text=' '
	votes={}

	def __init__(self,review_id,business_id,user_id,stars,text,votes):
		self.review_id=review_id		
		self.business_id=business_id
		self.user_id=user_id
		self.stars=stars
		self.text=text
		self.votes=votes
	def __init__(self,db,review_id):
		review=db.review
		r=review.find_one({"review_id":review_id})
		self.review_id=review_id		
		self.business_id=r['business_id']
		self.user_id=r['user_id']
		self.stars=r['stars']
		self.text=r['text']
		self.votes=r['votes']


	def Set_Business_Id(self,business_id):
		self.business_id=business_id
	def Get_Business_Id(self):
		return self.business_id

	def Set_User_Id(self,user_id):
		self.user_id=user_id
	def Get_User_Id(self):
		return self.user_id

	def Set_Stars(self,stars):
		self.stars=stars
	def Get_Stars(self):
		return self.stars

	def Set_Text(self,text):
		self.text=text
	def Get_Text(self):
		return self.text

	def Set_Votes(self,votes):
		self.votes=votes
	def Get_Votes(self):
		return self.votes


