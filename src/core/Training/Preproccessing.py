# -*- coding: utf-8 -*-
import logging, gensim, bz2, nltk
from ..Entities.Reviews import Reviews
from pymongo import MongoClient
from unicodedata import category
from gensim import corpora,models
from collections import defaultdict
from nltk.stem.snowball import SnowballStemmer
from gensim.corpora import dictionary
from StreamModel import StreamModel

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.root.level=logging.INFO
client = MongoClient('localhost', 27017)
db = client.yelp

streamerhack=StreamModel(db)
dictionary = corpora.Dictionary(doc for doc in streamerhack )
dictionary.save('../Data/myDict.dict')
dictionary=corpora.Dictionary.load('../Data/myDict.dict')

print("Dictionary saved SUCESSFULLY!!!!")
streamerhack=StreamModel(db)
corpus = [dictionary.doc2bow(doc) for doc in streamerhack]
print("Ready to S E R I A L I Z E . . .")
corpora.MmCorpus.serialize('..Data/myMM.mm', corpus,progress_cnt=1000)

