from pymongo import MongoClient
from ..Entities.UserGroup import UserGroup
from ..Entities.BusinessGroup import BusinessGroup
from ..Entities.Association import Association
from ..Entities.User import User
from ..Entities.Business import Business
import itertools

def Associate(db):
	
	cursorU=db.userGroup.find()
	for u in cursorU:
		ug=UserGroup.Cursor_Constructor(u)
		cursorB=db.businessGroup.find()
		inDB=False
		for b in cursorB:
			bg=BusinessGroup.Cursor_Constructor(b)
			if set(ug.Get_Topics())==set(bg.Get_Topics()):
				
				assoc=Association.Default_Constructor(u['_id'],b['_id'])
				assoc.toDB(db)
				inDB=True
				break
		if not inDB:
			cursorC=db.businessGroup.find()
			found=False
			for b in cursorC:

				if found:
					break
				bg=BusinessGroup.Cursor_Constructor(b)
				for el in set(itertools.combinations(ug.Get_Topics(), 2)):
						if set(el).issubset(set(bg.Get_Topics())):

							assoc=Association.Default_Constructor(u['_id'],b['_id'])
							assoc.toDB(db)
							found=True
							break
			
		
client = MongoClient('localhost', 27017)
db = client.yelp

Associate(db)

