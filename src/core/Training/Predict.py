import logging
from pymongo import MongoClient
from ..Entities.Reviews import Reviews
from ..Entities.User import User
from ..Entities.UserGroup import UserGroup
from ..Entities.BusinessGroup import BusinessGroup
from gensim.models import LdaModel
from gensim import corpora
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk import word_tokenize,sent_tokenize
from unicodedata import category
from pprint import pprint  
from collections import Counter
import operator
from operator import itemgetter



class Predict():
	def __init__(self):
		dictionary_path = "../Data/myDict.dict"
		lda_model_path = "../Data/lda_model_topics.lda"
		self.dictionary = corpora.Dictionary.load(dictionary_path)
		self.lda = LdaModel.load(lda_model_path)
	
	def load_stopwords(self):
		stopwords = {}
		with open('../Data/stop-words.txt', 'rU') as f:
			for line in f:
				stopwords[line.strip()] = 1

		return stopwords
	def stem(self, new_review):
		stopwords = self.load_stopwords()
		words = []
		stemmer=SnowballStemmer("english")
		texT=new_review.lower().split()
		for word in texT:
			if word not in stopwords:
				word=''.join(ch for ch in word if category(ch)[0] != 'P')
				word=stemmer.stem(word)
				words.append(word)
   		return words

	def run(self, new_review, k):
		words = self.stem(new_review)
		new_review_bow = self.dictionary.doc2bow(words)
		new_review_lda = self.lda[new_review_bow] 
		
		p=sorted(new_review_lda, key=itemgetter(1))
		p.reverse()
		resk=[]
		res=p[0:3]
		for i in res:
			resk.append(i[0])
		return resk

	

	def main():
			
		classify_U={}
		classify_B={}
		Users={}
		Businesses={}
	
		r=db.review.find()
		j=0
		for rev in r:
			top_k=predict.run(rev['text'],3)
	
			for i in top_k:
				classify_U.setdefault(rev['user_id'],[]).append(i)
				classify_B.setdefault(rev['business_id'],[]).append(i)
		
			count_U=dict(Counter(classify_U[rev['user_id']]))
			count_B=dict(Counter(classify_B[rev['business_id']]))
			Users[rev['user_id']]=count_U
			Businesses[rev['business_id']]=count_B
			if j%10000==0:
        	        	print("Review :"),j
			j+=1
	
		for key,v in Users.items():
			sor=[]
			sort_v = sorted(v.items(), key=operator.itemgetter(1))	
			sort_v.reverse()
			sorted_v=sort_v[0:3]
			
			for i in range(0,len(sorted_v)):
				sor.append(sorted_v[i][0])
			Users[key]=sor
		
		my_ugs=[]
		
		for key,value in Users.items():
					
			match=False
			for ug in my_ugs:
				if ug.Topics_Match(value)==True:
					ug.Add_User(key)
					match=True
					break	
							
			if match==False:
				ug=UserGroup.Default_Constructor(key,value)			
				my_ugs.append(ug)
			
		
	
		
		for key,v in Businesses.items():
			sor=[]
			sort_v = sorted(v.items(), key=operator.itemgetter(1))	
			sort_v.reverse()
			sorted_v=sort_v[0:3]
			
			for i in range(0,len(sorted_v)):
				sor.append(sorted_v[i][0])
			Businesses[key]=sor
		
		my_bgs=[]
		
		for key,value in Businesses.items():
					
			match=False
			for bg in my_bgs:
				if bg.Topics_Match(value)==True:
					bg.Add_Business(key)
					match=True
					break	
							
			if match==False:
				bg=BusinessGroup.Default_Constructor(key,value)			
				my_bgs.append(bg)
				
	
	
	

		print ("Ready to write to DB")
		for ug in my_ugs:
			ug.toDB(db)
			
		for bg in my_bgs:
			bg.toDB(db)
			
	

	
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)	
client = MongoClient('localhost', 27017)
db = client.yelp
predict = Predict()
predict.main(db)








