# -*- coding: utf-8 -*-
from ..Entities.Reviews import Reviews
from pymongo import MongoClient
from unicodedata import category
from gensim import corpora, models, similarities
from nltk.stem.snowball import SnowballStemmer



class StreamModel(object):
    
        def __init__(self,db):
            self.stemmer=SnowballStemmer("english")
            self.stoplist=set()
            f = open('../Data/stop-words.txt')
            for word in f.read().split():
                self.stoplist.add(word)
                self.db = db
                self.reviews=self.db.review.find()
                
        def __iter__(self):
            print("ITER")
            i=0
            for r in self.reviews:
                texT=r['text'].lower().split()
                doc=[]
                if i%10000==0:
                    print i
                for word in texT:
                    if word not in self.stoplist:
                        word=''.join(ch for ch in word if category(ch)[0] != 'P')
                        word=self.stemmer.stem(word)
                        doc.append(word)
                i+=1
                yield doc

			
