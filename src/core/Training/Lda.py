# -*- coding: utf-8 -*-
import logging, gensim, bz2, nltk
from ..Entities.Reviews import Reviews
from gensim import corpora, models, similarities


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


dictionary = corpora.Dictionary.load('../Data/myDict.dict') 
corpus = corpora.MmCorpus('../Data/myMM.mm')

#apply lda model
lda = gensim.models.ldamodel.LdaModel(corpus=corpus, id2word=dictionary, num_topics=20, update_every=0, chunksize=1000, passes=5)
lda.print_topics(20)

#save lda topics to file
lda_model_path = "../Data/lda_model_topics.lda"
lda.save(lda_model_path)


	



