from gensim.models import LdaModel
from ..Entities.Topic import Topic
import pymongo
from pymongo import MongoClient

lda_model_path = "../Data/lda_model_topics.lda"
lda = LdaModel.load(lda_model_path)
client = MongoClient('localhost', 27017)
db = client.yelp

for i in range (0,20):
	a=[]
	for j in range (0,3):
		a.append(lda.show_topic(i, topn=3)[j][1])
	t=Topic.Default_Constructor(i,a)
	t.toDB(db)
	

