#run python -m  core.Classification.Classifier
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from Entities.User import User
from Entities.Business import Business
from Entities.UserGroup import UserGroup
from Entities.BusinessGroup import BusinessGroup
from Entities.Association import Association


class Classifier: 
	
	def Classify(self, db, UserId):
		userGroupId=db.userGroup.find_one({'users':{'$eq':UserId}})['_id']
		userGroupId=ObjectId(userGroupId)
		assoc=Association.Cursor_Constructor(db.association.find_one({'userGroupId':userGroupId}))
		return [Business for Business in assoc.Fetch_BusinessGroup(db).Fetch_Business_Objects(db)]			
		
		
		


client = MongoClient('localhost', 27017)
db = client.yelp	
classifier=Classifier()
#print [b.Get_Name() for b in classifier.Classify(db, 'KbJBfIrCe8EJLpX4t6B2eg')]

