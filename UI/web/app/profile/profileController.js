var yelpControllers = angular.module('Yelp.Controllers');

yelpControllers.controller('ProfileController', function ($scope, $http, $stateParams) {

    $scope.user = undefined;
    $scope.loading = false;

    $scope.loadUser = function () {
    $scope.loading = true;
       $http({
           url: 'http://127.0.0.1:8000/users/userinfo/.json',
           method: "GET",
           params: {user_id: $stateParams.u_id}
       })
          .success(function(userInfo) {
            $scope.user = userInfo;
            $scope.loading = false;
       });

    }

    $scope.loadUser();

    $scope.isEmpty = function (obj) {
        for (var i in obj) if (obj.hasOwnProperty(i)) return false;
        return true;
    };

});