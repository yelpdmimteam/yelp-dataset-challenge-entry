
// declare the main application module
var yelpModule = angular.module('Yelp', [
    'ui.router',
    'ngMap',
    'ngResource',
    'ui.bootstrap',
    'Yelp.Controllers'
]);

/**
 * Configure $http service
 */
yelpModule.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);

/**
 * Configure application states (application pages)
 */
yelpModule.config(function ($stateProvider, $urlRouterProvider) {

    // specify the default url to be /intro
    $urlRouterProvider.otherwise("/intro");

    // configure the states
    $stateProvider
      .state('intro', {
          url: "/intro",
          templateUrl: "./app/intro/intro.html",
          controller: 'IntroController'
      });
    $stateProvider
      .state('business-list', {
          url: "users/{u_id}/{u_name}/businesses",
          templateUrl: "./app/businessList/business-list.html",
          controller: 'BusinessListController',
          controllerAs: 'businessListController'
      });
    $stateProvider
        .state('similarUsers', {
            url: "/similarUsers/{u_id}",
            templateUrl: "./app/similarUsers/similar_users.html",
            controller: 'SimilarUsersController',
            controllerAs: 'similarUsersController'
        });
    $stateProvider
        .state('profile', {
            url: "/user/{u_id}",
            templateUrl: "./app/profile/profile.html",
            controller: 'ProfileController',
            controllerAs: 'profileController'
        });
});

