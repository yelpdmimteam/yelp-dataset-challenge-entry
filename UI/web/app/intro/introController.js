var yelpControllers = angular.module('Yelp.Controllers');

yelpControllers.controller('IntroController', function ($scope,$state,$http) {

    $scope.error = undefined;
    $scope.loading = false;

    $scope.start = function (userId) {
        $scope.error = undefined;

        if(userId === undefined || userId === ''){
            $scope.error = 'The above field is required.'
            return;
        }

        $scope.loadUser(userId);
    }

    $scope.loadUser = function (userId) {
        $scope.loading = true;
        $http({
            url: 'http://127.0.0.1:8000/users/userinfo/.json',
            method: "GET",
            params: {user_id: userId}
        })
        .success(function(userInfo) {
            $scope.user = userInfo;
            $scope.loading = false;
        })
        .error(function(userInfo) {
            $scope.error = 'The user with the given identifier does not exist.';
            $scope.loading = false;
        });
    }

    $scope.findBusinesses = function () {
        $state.go('business-list', { u_id: $scope.user.user_id, u_name:$scope.user.user_name });
    }

    $scope.findSimilarUsers = function() {
        $state.go('similarUsers', { u_id: $scope.user.user_id });
    }

    $scope.viewProfile = function() {
            $state.go('profile', { u_id: $scope.user.user_id });
        }


});