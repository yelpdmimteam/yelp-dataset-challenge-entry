var yelpControllers = angular.module('Yelp.Controllers');

yelpControllers.controller('SimilarUsersController', function ($scope, $http, $stateParams) {

    $scope.similarUsers = [];
    $scope.loading = false;
    $scope.loadSimilarUsers = function () {
    $scope.loading = true;
      $http({
              url: 'http://127.0.0.1:8000/users/userinfo/similarusers/.json',
              method: "GET",
              params: {user_id: $stateParams.u_id}
          })
           .success(function(similarUsers) {
                $scope.similarUsers = similarUsers;
                $scope.loading = false;
           })
           .error(function() {
               $scope.loading = false;
           });
      }

    $scope.loadSimilarUsers();

});