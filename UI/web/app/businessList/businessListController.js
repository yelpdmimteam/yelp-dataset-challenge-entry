var yelpControllers = angular.module('Yelp.Controllers');

yelpControllers.controller('BusinessListController', function ($scope, $http, $stateParams) {
    $scope.businesses = undefined;
    $scope.user = {user_id:$stateParams.u_id,user_name:$stateParams.u_name};
    $scope.selected = undefined;
    $scope.loading = false;
    $scope.loadBusinessRecommendations = function(){
     $scope.loading = true;
        $http({
            url: 'http://127.0.0.1:8000/businesses/.json',
            method: "GET",
            params: {user_id: $stateParams.u_id}
        })
           .success(function(businesses) {
             $scope.businesses = businesses;
             $scope.selected = $scope.businesses[0];
             $scope.loading = false;

        });
    }

    $scope.select=function(business) {
        $scope.selected=business;
    }

    $scope.loadBusinessRecommendations();

});
